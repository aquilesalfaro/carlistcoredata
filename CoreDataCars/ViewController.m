//
//  ViewController.m
//  CoreDataCars
//
//  Created by user on 11/29/17.
//  Copyright © 2017 cop2654.mdc.edu. All rights reserved.
//

#import "ViewController.h"
#import "Vehicle+CoreDataClass.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *txtMake;
@property (weak, nonatomic) IBOutlet UITextField *txtModel;
@property (weak, nonatomic) IBOutlet UITextField *txtMPG;
@property (weak, nonatomic) IBOutlet UITextField *txtYear;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // get hold of the AppDelegate
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    context = appDelegate.persistentContainer.viewContext;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveRecord:(UIButton *)sender {
    Vehicle *myCar = [[Vehicle alloc]initWithContext:context];
    [myCar setValue:self.txtMake.text forKey:@"make"];
    [myCar setValue:self.txtModel.text forKey:@"model"];
    
    // format string into a decimal before commiting
    NSNumberFormatter * f = [[NSNumberFormatter alloc]init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    NSNumber *myMPG = [f numberFromString:self.txtMPG.text];
    [myCar setValue:myMPG forKey:@"mpg"];
    
    // format to appropriate data type
    [f setNumberStyle:NSNumberFormatterNoStyle];
    NSNumber *myYear = [f numberFromString:self.txtYear.text];
    [myCar setValue:myYear forKey:@"year"];
    
    NSLog(@"My Car: %@", myCar);
    
    // zero out field
    self.txtMake.text = @"";
    self.txtModel.text = @"";
    self.txtMPG.text = @"";
    self.txtYear.text = @"";
    
    NSError *error;
    [context save:&error];
    
    self.lblStatus.text = @"Car Saved";
    
}
- (IBAction)findRecord:(UIButton *)sender {
    /*NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Vehicle"];
     NSError *error = nil;
     NSArray *objects = [context executeFetchRequest:request error:&error];
     
     Vehicle *myCar;
     
     if ([objects count] == 0){
     NSLog(@"Error fetching Vehicle objects: %@\n%@", [error localizedDescription], [error userInfo]);
     self.lblStatus.text = @"No Matches";
     
     }else{
     myCar = objects[0];
     self.txtMake.text = [myCar valueForKey:@"make"];
     self.txtModel.text = [myCar valueForKey:@"model"];
     self.txtMPG.text = [[myCar valueForKey:@"mpg"]stringValue];
     self.txtYear.text = [[myCar valueForKey:@"year"]stringValue];
     
     self.lblStatus.text = [NSString stringWithFormat:@"%lu matches found", (unsigned long)[objects count]];
     }*/
    
    NSEntityDescription* entityDesc = [NSEntityDescription entityForName:@"Vehicle" inManagedObjectContext:context];
    
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    NSPredicate* pred = [NSPredicate predicateWithFormat:@"(make = %@)", self.txtMake.text];
    [request setPredicate:pred];
    
    Vehicle* myCar;
    
    NSError *error = nil;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    
    if ([objects count] == 0){
        NSLog(@"Error fetching Vehicle objects: %@\n%@", [error localizedDescription], [error userInfo]);
        self.lblStatus.text = @"No Matches";
        
    }else{
        myCar = objects[0];
        self.txtMake.text = [myCar valueForKey:@"make"];
        self.txtModel.text = [myCar valueForKey:@"model"];
        self.txtMPG.text = [[myCar valueForKey:@"mpg"]stringValue];
        self.txtYear.text = [[myCar valueForKey:@"year"]stringValue];
        
        self.lblStatus.text = [NSString stringWithFormat:@"%lu matches found", (unsigned long)[objects count]];
    }
    
    
}
- (IBAction)dismissKeyboard:(id)sender {
    
}
- (IBAction)clearFields:(UIButton *)sender {
    self.txtMake.text = @"";
    self.txtModel.text = @"";
    self.txtMPG.text = @"";
    self.txtYear.text = @"";
    
    self.lblStatus.text = @"";
    
}
- (IBAction)deleteRecord:(id)sender {
    NSEntityDescription* entityDesc = [NSEntityDescription entityForName:@"Vehicle" inManagedObjectContext:context];
    
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    NSPredicate* pred = [NSPredicate predicateWithFormat:@"(make = %@)", self.txtMake.text];
    [request setPredicate:pred];
    
    Vehicle* myCar;
    
    
    
    NSError *error = nil;
    NSArray *objects = [context executeFetchRequest:request error:&error];

    if ([objects count] == 0){
        NSLog(@"Error fetching Vehicle objects: %@\n%@", [error localizedDescription], [error userInfo]);
        self.lblStatus.text = @"No Matches";
        
    }else{
    myCar = objects[0];
    [context deleteObject:myCar];
    [context save:&error];
    
    // zero out the ui fields
    self.txtMake.text = @"";
    self.txtModel.text = @"";
    self.txtMPG.text = @"";
    self.txtYear.text = @"";
    
    self.lblStatus.text = @"Vehicle Deleted";
    }
    
}


@end

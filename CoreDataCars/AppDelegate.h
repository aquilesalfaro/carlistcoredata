//
//  AppDelegate.h
//  CoreDataCars
//
//  Created by user on 11/29/17.
//  Copyright © 2017 cop2654.mdc.edu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

@property (strong, nonatomic) NSManagedObjectContext *context;

@property (strong, nonatomic) UIWindow *window2;

@property (readonly, strong) NSPersistentContainer *persistentContainer2;

@property (strong, nonatomic) NSManagedObjectContext *context2;

- (void)saveContext;


@end

